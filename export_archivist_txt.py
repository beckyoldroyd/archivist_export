#!/usr/bin/env python3

"""
Python 3: Web scraping using selenium
    - Download tq.txt and qv.txt files from Archivist Instruments, rename: prefix.tqlinking.txt, prefix.qvmapping.txt
    - Download tv.txt and dv.txt files from Archivist Datasets, rename: prefix.tvlinking.txt, prefix.dv.txt
"""

import pandas as pd
import time
import sys
import os
import re

from mylib import get_driver, url_base, archivist_login_all, get_names, get_base_url


driver = get_driver()


def isNaN(num):
    return num != num


def archivist_download_txt(df_base_urls, output_prefix_dir, output_type_dir, uname, pw):
    """
    Loop over prefix, downloading txt files
    """
    # Log in to all
    urls = df_base_urls["base_url"].to_list()
    print("URLs is {}".format(urls))
    ok = archivist_login_all(driver, urls, uname, pw)

    for index, row in df_base_urls.iterrows():
        print(f"Dataset = {row['Dataset']}")
        base = url_base(row["base_url"])
        print('Working on export txt: instrument="{}", dataset="{}" from "{}" with full URL "{}"'.format(row["Instrument"], row["Dataset"], base, row["base_url"]))
        if not ok[base]:
            print("host {} was not available, skipping".format(base))
            continue

        if pd.isnull(row["Dataset"]):
            file_name = row["Instrument"]
        else:  
            file_name = row["Dataset"]

        output_dir = os.path.join(output_prefix_dir, file_name)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_tq_dir = os.path.join(output_type_dir, "tqlinking",)
        if not os.path.exists(output_tq_dir):
            os.makedirs(output_tq_dir)

        output_qv_dir = os.path.join(output_type_dir, "qvmapping",)
        if not os.path.exists(output_qv_dir):
            os.makedirs(output_qv_dir)

        output_tv_dir = os.path.join(output_type_dir, "tvlinking",)
        if not os.path.exists(output_tv_dir):
            os.makedirs(output_tv_dir)

        output_dv_dir = os.path.join(output_type_dir, "dv",)
        if not os.path.exists(output_dv_dir):
            os.makedirs(output_dv_dir)

        print("Getting " + row["Instrument"] + "/tq.txt")
        tq_url = os.path.join(row["base_url"], "instruments", row["Instrument"] + "/tq.txt")
        driver.get(tq_url)

        time.sleep(1)
        print("  Downloading " + row["Instrument"] + "/tq.txt")
        tq_content = driver.find_element_by_xpath("html/body").text

        if tq_content:
            with open(os.path.join(output_dir, row["Instrument"] + ".tqlinking.txt"), "w") as f:
                try:
                    f.write(tq_content.replace(" ", "\t"))
                except :
                    print("Could not download tq : ", row["Instrument"])
                    continue
            with open(os.path.join(output_tq_dir, row["Instrument"] + ".tqlinking.txt"), "w") as f:
                try:
                    f.write(tq_content.replace(" ", "\t"))
                except :
                    print("Could not download tq : ", row["Instrument"])
                    continue
            time.sleep(3)

        print("Getting " + row["Instrument"] + "/qv.txt")
        try:
            qv_url = os.path.join(row["base_url"], "instruments", row["Instrument"] + "/qv.txt")
        except:
            print("qv for: ", qv_url, " not found")
        driver.get(qv_url)

        time.sleep(1)
        print("  Downloading " + row["Instrument"] + "/qv.txt")
        qv_content = driver.find_element_by_xpath("html/body").text

        if qv_content:
            with open(os.path.join(output_dir, row["Instrument"] + ".qvmapping.txt"), "w") as f:
                f.write(qv_content.replace(" ", "\t"))
            with open(os.path.join(output_qv_dir, row["Instrument"] + ".qvmapping.txt"), "w") as f:
                f.write(qv_content.replace(" ", "\t"))
            time.sleep(3)

        # dataset: from prefix find id
        datasets_url = os.path.join(row["base_url"], "datasets")
        print(datasets_url)

        if datasets_url is not None:

            driver.get(datasets_url)
            time.sleep(10)

            # find the input box
            inputElement = driver.find_element_by_xpath("//input[@placeholder='Search for...']")
            print(f"Instrument = {row['Instrument']}")
            if pd.isnull(row["Dataset"]):
                inputElement.send_keys(row["Instrument"])
            else:  
                inputElement.send_keys(row["Dataset"]) 
            time.sleep(1)

            # locate id and link
            trs = driver.find_elements_by_xpath("html/body/div/div/div/div/table/tbody/tr")
            print("This page has {} rows".format(len(trs)))

            study_ids = []
            for i in range(1, len(trs)):
                # row 0 is header: tr has "th" instead of "td"
                tr = trs[i]

                # column 0 (first column) is "ID"
                study_ids.append(tr.find_elements_by_xpath("td")[0].text)

            for study_id in study_ids:
                print("Getting " + study_id + "/tv.txt")
                try:
                    tv_url = os.path.join(datasets_url, study_id + "/tv.txt")
                except:
                    "Cound not find: ", tv_url
                    continue
                driver.get(tv_url)
                time.sleep(10)
                print("  Downloading " + study_id + "/tv.txt")
                tv_content = driver.find_element_by_xpath("html/body").text

                if tv_content:
                    with open(os.path.join(output_dir, file_name + ".tvlinking.txt"), "w") as f:
                        f.write(tv_content.replace(" ", "\t"))
                    with open(os.path.join(output_tv_dir, file_name + ".tvlinking.txt"), "w") as f:
                        f.write(tv_content.replace(" ", "\t"))

                time.sleep(3)

                print("Getting " + study_id + "/dv.txt")
                dv_url = os.path.join(datasets_url, study_id + "/dv.txt")
                driver.get(dv_url)
                time.sleep(10)
                print("  Downloading " + study_id + "/dv.txt")
                dv_content = driver.find_element_by_xpath("html/body").text
                # Chrome does not put a <pre> tag in empty page
                # (Firefox instead gives empty string for dv_content)

                if dv_content:
                    with open(os.path.join(output_dir, file_name + ".dv.txt"), "w") as f:
                        f.write(dv_content.replace(" ", "\t"))
                    with open(os.path.join(output_dv_dir, file_name + ".dv.txt"), "w") as f:
                        f.write(dv_content.replace(" ", "\t"))
                time.sleep(3)

    driver.quit()

 
def get_txt(df, output_prefix_dir, output_type_dir, uname, pw):
    """
    Export txt files to output dir
    """

    df_base_urls = get_base_url(df)

    archivist_download_txt(df_base_urls, output_prefix_dir, output_type_dir, uname, pw)
           

def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    main_dir = "export_txt"
    output_prefix_dir = os.path.join(main_dir, "txt_by_prefix")
    if not os.path.exists(output_prefix_dir):
        os.makedirs(output_prefix_dir)
    output_type_dir = os.path.join(main_dir, "txt_by_type")
    if not os.path.exists(output_type_dir):
        os.makedirs(output_type_dir)

    # Hayley's txt as dataframe
    df = pd.read_csv("Prefixes_to_export.txt", sep="\t")

    get_txt(df, output_prefix_dir, output_type_dir, uname, pw)


if __name__ == "__main__":
    main()

